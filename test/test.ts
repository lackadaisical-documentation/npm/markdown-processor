/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
'use strict'

import test, { Macro } from 'ava'
import fs from 'fs'
import os from 'os'

import processMarkdown, { documentContent, hashString, fixFootnoteCollision } from '../src/index'
import makeImgPathsAbsolute from '../src/util/make-img-paths-absolute'

// eslint-disable-next-line no-undef
process.chdir('./test')

test('processMarkdown works on an array of files', async (t) => {
  const functionOutput: documentContent = await processMarkdown([
    './multiple-markdown-files/001.md',
    './multiple-markdown-files/002.md',
  ])
  const simplifiedBinder = [
    fs.readFileSync('./multiple-markdown-files/001.md').toString().trim(),
    fs.readFileSync('./multiple-markdown-files/002.md').toString().trim(),
  ]
  const simulatedOutput = simplifiedBinder.join(`${os.EOL}${os.EOL}`) + os.EOL + os.EOL
  t.is(functionOutput.content, simulatedOutput)
})

test('processMarkdown appends correct number of newlines for no EOF newline', async (t) => {
  const functionOutput: documentContent = await processMarkdown([
    './markdown-files-without-eof-newline/001.md',
    './markdown-files-without-eof-newline/002.md',
  ])
  const simplifiedBinder = [
    fs.readFileSync('./markdown-files-without-eof-newline/001.md'),
    fs.readFileSync('./markdown-files-without-eof-newline/002.md'),
  ]
  const simulatedOutput = simplifiedBinder.join(`${os.EOL}${os.EOL}`) + os.EOL + os.EOL
  t.is(functionOutput.content, simulatedOutput)
})

test('hashString produces an appropriate hash', (t) => {
  const output = hashString('this is a string')
  t.is('1253166132', output.toString())
})

test('FixFootnoteCollision returns appropriately fixed footnotes', (t) => {
  const filename = 'test/file.name'
  const output = fixFootnoteCollision(`String with a footnote[^FN]`, filename)
  t.is('String with a footnote[^-1070315821FN]', output)
})

test.todo('Can read ISO-8859-1')
test.todo('Can read UTF-8')
test.todo('Can read UTF-8 + BOM')
test.todo('Can read UTF-16 LE')
test.todo('Can read UTF-16 BE')
test.todo('Can read UTF-32 LE')
test.todo('Can read UTF-32 BE')

/**
 * @param {string}  t         Test title
 * @param {string}  input     test input
 * @param {string}  expected  expected output of test
 */
const testMakeImagePathsAbsolute: Macro<[string, string]> = (t, input, expected) => {
  t.is(makeImgPathsAbsolute('/home/foo/bar', input), expected)
}

testMakeImagePathsAbsolute.title = (
  providedTitle = 'Test image path resolution for',
  input: string,
  expected: string
) => `${providedTitle} ${input} = ${expected}`.trim()

test(testMakeImagePathsAbsolute, '![Test](./test.png)', '![Test](/home/foo/bar/test.png)')
test(testMakeImagePathsAbsolute, '![Test](test.png)', '![Test](/home/foo/bar/test.png)')
test(testMakeImagePathsAbsolute, '![Test](../test.png)', '![Test](/home/foo/test.png)')
test(testMakeImagePathsAbsolute, '![Test](http://some-url.com/test.png)', '![Test](http://some-url.com/test.png)')
test(testMakeImagePathsAbsolute, '![Test](ftp://some-url.com/test.png)', '![Test](ftp://some-url.com/test.png)')
test(testMakeImagePathsAbsolute, '![Test](file://./test.png)', '![Test](file://./test.png)')
// Test for captions and alt-text
test(testMakeImagePathsAbsolute, '![Test](file://./test.png "Alt Text")', '![Test](file://./test.png "Alt Text")')
test(testMakeImagePathsAbsolute, '![](file://./test.png "Alt Text")', '![](file://./test.png "Alt Text")')
test(testMakeImagePathsAbsolute, '![](./test.png "Alt Text")', '![](/home/foo/bar/test.png "Alt Text")')
test(testMakeImagePathsAbsolute, '![Test](../test.png "Alt Text")', '![Test](/home/foo/test.png "Alt Text")')
test(
  testMakeImagePathsAbsolute,
  '![Test](ftp://some-url.com/test.png "Alt Text")',
  '![Test](ftp://some-url.com/test.png "Alt Text")'
)
// Test for the additional meta data of an image
test(
  testMakeImagePathsAbsolute,
  '![Test](file://./test.png "Alt Text"){. class}',
  '![Test](file://./test.png "Alt Text"){. class}'
)
test(
  testMakeImagePathsAbsolute,
  '![](file://./test.png "Alt Text"){. class}',
  '![](file://./test.png "Alt Text"){. class}'
)
test(
  testMakeImagePathsAbsolute,
  '![](./test.png "Alt Text"){. class}',
  '![](/home/foo/bar/test.png "Alt Text"){. class}'
)
test(
  testMakeImagePathsAbsolute,
  '![Test](../test.png "Alt Text"){. class}',
  '![Test](/home/foo/test.png "Alt Text"){. class}'
)
test(
  testMakeImagePathsAbsolute,
  '![Test](ftp://some-url.com/test.png "Alt Text"){. class}',
  '![Test](ftp://some-url.com/test.png "Alt Text"){. class}'
)
// Minimal examples
test(testMakeImagePathsAbsolute, '![](./test.png)', '![](/home/foo/bar/test.png)')

// Reference-style images:
/**
 * @param {string}  t         Test title
 * @param {string}  input     test input
 * @param {string}  expected  expected output of test
 */
 const testMakeRefImagePathsAbsolute: Macro<[string, string]> = (t, input, expected) => {
  t.is(makeImgPathsAbsolute('/home/foo/bar', input), expected)
}

testMakeRefImagePathsAbsolute.title = (
  providedTitle = 'Test reference-style image path resolution for',
  input: string,
  expected: string
) => `${providedTitle} ${input} = ${expected}`.trim()

test(testMakeRefImagePathsAbsolute, '[Test]: ./test.png', '[Test]: /home/foo/bar/test.png')
test.failing(testMakeRefImagePathsAbsolute, '[Test]: test.png', '[Test]: /home/foo/bar/test.png') // Current regex will not match 'file.ext' without a relative path. Oops!
test(testMakeRefImagePathsAbsolute, '[Test]: ../test.png', '[Test]: /home/foo/test.png')
test(testMakeRefImagePathsAbsolute, '[Test]: http://some-url.com/test.png', '[Test]: http://some-url.com/test.png')
test(testMakeRefImagePathsAbsolute, '[Test]: ftp://some-url.com/test.png', '[Test]: ftp://some-url.com/test.png')
test(testMakeRefImagePathsAbsolute, '[Test]: file://./test.png', '[Test]: file://./test.png')
// Test for captions and alt-text
test(testMakeRefImagePathsAbsolute, '[Test]: file://./test.png "Alt Text"', '[Test]: file://./test.png "Alt Text"')
test(testMakeRefImagePathsAbsolute, '[Test]: ../test.png "Alt Text"', '[Test]: /home/foo/test.png "Alt Text"')
test(
  testMakeRefImagePathsAbsolute,
  '[Test]: ftp://some-url.com/test.png "Alt Text"',
  '[Test]: ftp://some-url.com/test.png "Alt Text"'
)
// Test for the additional metadata of an image
test(
  testMakeRefImagePathsAbsolute,
  '[Test]: file://./test.png "Alt Text" {. class}',
  '[Test]: file://./test.png "Alt Text" {. class}'
)
// URIs may be in angular brackets
test(
  testMakeRefImagePathsAbsolute,
  '[Test]: <file://./test.png> "Alt Text" {. class}',
  '[Test]: file://./test.png "Alt Text" {. class}'
)
test(
  testMakeRefImagePathsAbsolute,
  '[Test]: ./test.png "Alt Text" {. class}',
  '[Test]: /home/foo/bar/test.png "Alt Text" {. class}'
)
test(
  testMakeRefImagePathsAbsolute,
  '[Test]: ../test.png "Alt Text" {. class}',
  '[Test]: /home/foo/test.png "Alt Text" {. class}'
)
test(
  testMakeRefImagePathsAbsolute,
  '[Test]: ftp://some-url.com/test.png "Alt Text" {. class}',
  '[Test]: ftp://some-url.com/test.png "Alt Text" {. class}'
)
