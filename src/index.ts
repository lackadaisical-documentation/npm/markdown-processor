/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 * BEGIN HEADER
 *
 * Contains:        Utility function
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This module concatenates and processes an array of Markdown files.
 *
 * END HEADER
 */

import { getFnExportRE } from '@lackadaisical/regular-expressions'
import { readFileAsyncEncoding } from '@lackadaisical/file-utils'
import chardet from 'chardet'
import os from 'os'
import path from 'path'

import makeImgPathsAbsolute from './util/make-img-paths-absolute'

export interface documentContent extends Record<string, string> {
  content: string
  encoding: string
}

/**
 * Returns a hash for a string.
 *
 * @param   {string}  str       String that we want a hash for.
 * @returns  {number}           The hash of the string.
 */
export function hashString(str: string): number {
  let hash = 0
  for (let i = 0; i < str.length; i++) {
    hash += Math.pow(str.charCodeAt(i) * 31, str.length - i)
    hash = hash & hash // Convert to 32bit integer
  }
  return hash
}

/**
 * Performs a regex over all Markdown footnotes to ensure that they are unique on a per-file basis.
 *
 * @param   {string}  content   Content that we want unique footnotes for.
 * @param   {string}  filename  Filename to generate a hash from.
 * @returns  {string}           Content with unique footnotes.
 */
export function fixFootnoteCollision(content: string, filename: string): string {
  // Extracts footnotes and gives them a unique ID per-file to prevent collisions.
  const fnRE = getFnExportRE()
  return content.replace(fnRE, (_match, p1: string, _offset, _string) => {
    return `[^${String(hashString(filename))}${p1}]`
  })
}

/**
 * Returns the concatenated content of Markdown files.
 * Performs logic on each file's content to ensure that footnotes are unique
 * and that image paths are absolute.
 *
 * @param   {Array}     files       List of files to process
 * @returns  {Object}               The concatenated output.
 *                                        .content = MarkDown content
 *                                        .encoding = encoding used
 */
export default async function processMarkdown(files: Array<string>): Promise<documentContent> {
  const contents: string[] = []
  const eol = os.EOL
  const promiseArr: Promise<string>[] = []
  const finalContents: documentContent = {
    content: '',
    encoding: '',
  }
  // Assume that the first file we encounter has the encoding we want to use for all files
  await chardet
    .detectFile(files[0])
    .then((encoding) => {
      if (typeof encoding === 'string') {
        finalContents.encoding = encoding === 'ISO-8859-1' ? 'UTF-8' : encoding
      } else {
        throw `Could not detect character encoding of ${path.basename(files[0])}.`
      }
    })
    .catch((err) => {
      throw err
    })

  for (const file of files) {
    promiseArr.push(readFileAsyncEncoding(file))
  }
  const fileContentArr = await Promise.all(promiseArr)

  fileContentArr.forEach((value, index) => {
    let fileContents = ''
    fileContents = makeImgPathsAbsolute(path.dirname(files[index]), value)
    if (!(fileContentArr.length === 1)) {
      fileContents = fixFootnoteCollision(fileContents, files[index])
    }
    contents.push(fileContents.trim() + `${eol}${eol}`)
  })

  finalContents.content = contents.join('')

  return finalContents
}
