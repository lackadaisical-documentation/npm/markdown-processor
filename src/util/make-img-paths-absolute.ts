/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 * BEGIN HEADER
 *
 * Contains:        Utility function
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This file contains a utility function to make image paths absolute.
 *
 * END HEADER
 */

import { getImageRE, getRefImageRE } from '@lackadaisical/regular-expressions'
import path from 'path'

/**
 * This function takes a Markdown string and replaces all occurrences of images
 * with an absolutised version.
 *
 * @param  {string} basePath  The basepath with which relative paths should be joined.
 * @param  {string} mdstring  The string to be altered.
 * @returns {string}          The altered mdstring value.
 */
export default function makeImgPathsAbsolute(basePath: string, mdstring: string): string {
  const imgRE = getImageRE(true)
  const refImgRE = getRefImageRE(true)
  /*
   *  We'll make use of path for file system URIs, and the URL() constructor
   *  For web links. We know that new URL() will throw a TypeError if the URL
   *  Is not valid, so we have two distinct cases: If URL does not throw, it's
   *  A valid URL and we can simply pass that one. But if it throws, use some
   *  Path-magic to convert it into an absolute path.
   *  Short explainer for "throwawayVariable": If we instantiate URL
   *  Without "new" it'll throw an error always. But if we simply use "new"
   *  There may be side effects. So we'll stuff it into an unused variable
   *  And disable the linter rule ...
   */
  let content = mdstring.replace(imgRE, (_match, p1: string, p2: string, p3: string, p4: string, _offset, _string) => {
    // Strip the image title from the second group.
    if (p3) p2 = p2.replace(`"${p3}"`, '').trim()
    try {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const throwawayVariable = new URL(p2)
    } catch (err) {
      p2 = path.resolve(basePath, p2)
    }
    //                     p1    |        p2        |     p3       |  p4
    // ReturnValue = '![Alt Text](/path/to/image.ext "Image Title"){Class}
    return `![${p1}](${p2}${p3 !== undefined ? ` "${p3}"` : ''})${p4 !== undefined ? p4 : ''}`
  })
  // Also handle reference-style image paths
  content = content.replace(refImgRE, (_match, p1: string, p2: string, p3: string, p4: string, _offset, _string) => {
    try {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const throwawayVariable = new URL(p2)
    } catch (err) {
      p2 = path.resolve(basePath, p2)
    }
    //                     p1     |       p2         |     p3       |  p4
    // ReturnValue = '[Reference]: /path/to/image.ext "Image Title" {Class}
    return `[${p1}]: ${p2}${p3 !== undefined ? ` "${p3}"` : ''}${p4 !== undefined ? ` {${p4}}` : ''}`
  })
  return content
}
