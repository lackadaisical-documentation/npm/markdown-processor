# @lackadaisical/markdown-processor

A module that concatenates and processes an array of Markdown files.

- Makes image paths absolute
- If multiple files are passed, prepends reference IDs with a hash of the filename to ensure unique IDs in the resulting output.

## Installation and Usage

npm:

`npm install @lackadaisical/markdown-processor`

Yarn:

`yarn add @lackadaisical/markdown-processor`

### processMarkdown

`processMarkdown (files)`

Where:

- `files` is :
  + an array of pathlike strings

Returns:

- Returns  {Object}               The concatenated output.
  + .content = MarkDown content
  + .encoding = encoding used
